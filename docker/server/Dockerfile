FROM debian:jessie
MAINTAINER Ondřej Šejvl

RUN groupadd -r sejvlond -g 1000 && useradd -r -g sejvlond -u 1000 sejvlond

# copy nedded files
COPY docker/locale.gen /etc/locale.gen

ENV ROOT_DIR /home/sejvlond/palladium/

COPY palladium/deb_dist/python3-palladium_0.9.1-1_all.deb /tmp/palladium.deb
RUN    apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get install -q -y --no-install-recommends \
            locales python3-all sudo bash-completion python3-pip \
            python3-docopt python3-flask python3-joblib python3-numpy g++ \
            python3-pandas python3-psutil python3-sqlalchemy python3-dev \
            python3-pkg-resources python3-scipy \
    && echo "LANG=en_US.utf8" > /etc/default/locale \
    && /usr/sbin/locale-gen \
    && mkdir -p "${ROOT_DIR}/" \
    && chown -R sejvlond:sejvlond "${ROOT_DIR}" \
    && echo "%sejvlond ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/sudo \
    && chmod 0440 /etc/sudoers.d/sudo \
    && dpkg -i /tmp/palladium.deb \
    && rm -f /tmp/palladium.deb \
    && pip3 install ujson scikit-learn tornado

EXPOSE 5000

ENV LANG en_US.UTF8
WORKDIR /home/sejvlond/palladium
USER sejvlond
VOLUME /home/sejvlond/palladium

ENV PALLADIUM_CONFIG=config.py
COPY src/model.db model.db
COPY src/config.py config.py
RUN    echo "cd ${ROOT_DIR}/server && python3 server.py & \n \
             cd ${ROOT_DIR} && pld-devserver" \
            > /tmp/run.sh \
    && chmod u+x /tmp/run.sh
ENTRYPOINT ["/bin/bash", "/tmp/run.sh"]
