from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import GaussianNB as Classificator
from sklearn.preprocessing import OneHotEncoder
from sklearn.base import TransformerMixin


class DenseTransformer(TransformerMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.params = {}

    def transform(self, x, y=None, **fit_params):
        return x.todense()

    def fit_transform(self, x, y=None, **fit_params):
        self.fit(x, y, **fit_params)
        return self.transform(x)

    def fit(self, x, y=None, **fit_params):
        return self

    def set_params(self, **params):
        self.params.update(params)
        return self

    def get_params(self, deep=True):
        return self.params


class CatToInt(TransformerMixin):
    def __init__(self, cat_values=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.params = dict(cat_values=cat_values)

    def __parse_cat_values(self):
        self.__cats = []
        for line in self.params['cat_values']:
            cat = {}
            for k, v in enumerate(line):
                cat[v] = k
            self.__cats.append(cat)

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None):
        self.__parse_cat_values()
        data = []
        for line in x:
            row = []
            for i, col in enumerate(line):
                row.append(self.__cats[i][col])
            data.append(row)
        return data

    def set_params(self, **params):
        self.params.update(params)
        return self

    def get_params(self, deep=True):
        return self.params

    def fit_transform(self, x, y=None):
        self.fit(x, y)
        return self.transform(x, y)


def model(**kwargs):
    classifier = Pipeline([
        ("cat2int", CatToInt()),
        ("encoder", OneHotEncoder()),
        ('to_dense', DenseTransformer()),
        ("clf", Classificator()),
    ])
    classifier.set_params(**kwargs)
    return classifier
