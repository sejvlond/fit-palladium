import tornado.ioloop
import tornado.web
from tornado import template


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = template.Loader(".")
        self.write(loader.load("index.html").generate())


application = tornado.web.Application([
    (r"/", MainHandler),
], debug=False)

if __name__ == "__main__":
    application.listen(8080)
    tornado.ioloop.IOLoop.current().start()
